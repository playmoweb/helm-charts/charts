# Helm Charts
This repository collects Jack's not official charts from their individual repos and automatically publish them to our Helm repo, located at //TODO add URL.

The charts collected and published currently by this repository are the following:

| Chart name   | Status       | Remark
|--------------|--------------|----------|
| [auto-deploy-app-with-node-selector](https://gitlab.com/playmoweb/helm-charts/auto-deploy-app-with-node-selector)
